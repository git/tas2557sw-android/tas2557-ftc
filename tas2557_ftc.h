/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2557_ftc.h
**
** Description:
**     header file for tas2557_ftc.c
**
** =============================================================================
*/

#ifndef TAS2557_FTC_H_
#define TAS2557_FTC_H_

#include <stdint.h>
#include <stdbool.h>

struct TSPKCharData {
	double nSpkTMax;
	double nSpkReTolPer;
	double nSpkReAlpha;

	double nPPC3_Re0;
	double nPPC3_FWarp;
	double nPPC3_Bl;
	double nPPC3_Mms;
	double nPPC3_RTV;
	double nPPC3_RTM;
	double nPPC3_RTVA;
	double nPPC3_SysGain;
	double nPPC3_DevNonlinPer;
	double nPPC3_PIG;

	double nReHi;
	double nReLo;
};

struct TFTCConfiguration {
	bool bVerbose;
	bool bLoadCalibration;
	unsigned int nCalibrationTime;
	double nPPC3_FS;

	struct TSPKCharData nTSpkCharDevA;
};

uint32_t tas2557_ftc(double t_cal, struct TFTCConfiguration *pFTCC);
int tas2557_chk(double t_cal, struct TFTCConfiguration *pFTCC);
#endif /* TAS2557_FTC_H_ */
