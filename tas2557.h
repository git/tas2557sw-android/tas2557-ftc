/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2557.h
**
** Description:
**     header file for tas2557.c
**
** =============================================================================
*/

#ifndef TAS2557_H_
#define TAS2557_H_

#include <stdint.h>
#include "tas2557_ftc.h"

/* 0000 0000 0BBB BBBB BPPP PPPP PRRR RRRR */

#define TAS2557_REG(book, page, reg)		((((unsigned int)book * 256 * 128) + \
						 ((unsigned int)page * 128)) + reg)

#define TAS2557_BOOK_ID(reg)			((unsigned char)(reg / (256 * 128)))
#define TAS2557_PAGE_ID(reg)			((unsigned char)((reg % (256 * 128)) / 128))
#define TAS2557_BOOK_REG(reg)			((unsigned char)(reg % (256 * 128)))
#define TAS2557_PAGE_REG(reg)			((unsigned char)((reg % (256 * 128)) % 128))

#define TILOAD_IOC_MAGIC   0xE0

#define TILOAD_IOCTL_SET_CHL			_IOW(TILOAD_IOC_MAGIC, 5, int)
#define TILOAD_IOCTL_SET_CONFIG			_IOW(TILOAD_IOC_MAGIC, 6, int)
#define TILOAD_IOCTL_SET_CALIBRATION	_IOW(TILOAD_IOC_MAGIC, 7, int)

#define RESULT_PASS			0x00000000 
#define RE1_FAIL_HI			0x00000001 
#define RE1_FAIL_LO			0x00000010 
#define RE1_CHK_MSK			0x00000011 

uint8_t tas2557_get_PGID(void);
void tas2557_mixer_command(char *pCommand, int nData);
uint32_t tas2557_coeff_read(uint32_t reg);
void tas2557_coeff_write(uint32_t reg, uint32_t data);
void tas2557_save_cal(struct TFTCConfiguration *pFTCC,
	double dev_a_re, uint32_t dev_a_rms_pow, uint32_t dev_a_t_limit,
	double t_cal, uint32_t,char * pFileName);
uint32_t check_spk_bounds(struct TFTCConfiguration *pFTCC, double re1);
void tas2557_load_calibration(int nCalibration);
void tas2557_open_bin(char * pFileName);
void tas2557_close_bin(void);
void tas2557_ftc_release(void);

#endif /* TAS2557_H_ */
